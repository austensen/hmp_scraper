import sys
sys.path.append('./code/')
from case_class import caseScrape

# put a list of cases here
cases = [

    "LT-075966-19/KI"
]

if __name__ == "__main__":
    scraper = caseScrape()

    for case in cases:
        scraper.getCaseData(case)
    
    scraper.quit()